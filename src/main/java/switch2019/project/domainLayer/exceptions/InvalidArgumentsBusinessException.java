package switch2019.project.domainLayer.exceptions;

public class InvalidArgumentsBusinessException extends RuntimeException {

    public InvalidArgumentsBusinessException(String message) {
        super(message);
    }
}
