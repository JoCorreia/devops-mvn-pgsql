package switch2019.project.domainLayer.repositoriesInterfaces;

import org.springframework.stereotype.Repository;
import switch2019.project.domainLayer.domainEntities.aggregates.account.Account;
import switch2019.project.domainLayer.domainEntities.aggregates.category.Category;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;

import java.util.List;
import java.util.Optional;

/**
 * The interface Account repository.
 */
@Repository
public interface IAccountRepository {

    //----------------------- NOVO ----------------------//

    Account save(Account account );

    List<Account> findAll();

    Optional<Account> findById(String id, String denomination);

    boolean existsById(AccountID accountID);

    long count();

    void delete(Account account);

    List<Account> findAllById(String description, String denomination,String id);
}