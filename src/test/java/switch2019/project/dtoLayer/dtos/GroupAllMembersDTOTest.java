package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GroupAllMembersDTOTest {

    @Test
    @DisplayName("GroupAllMembersDTO - Test Constructor with Parameters")
    void groupAllMembersDTO_ConstructorWithParametersTest() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        // Expected
        List<GroupMemberClearanceDTO> listGroupMemberClearanceDTO = new ArrayList<>();
        listGroupMemberClearanceDTO.add(groupMemberClearanceDTO);

        // Act

        GroupAllMembersDTO groupAllMembersDTO = new GroupAllMembersDTO(listGroupMemberClearanceDTO);

        // Assert
        assertEquals(listGroupMemberClearanceDTO, groupAllMembersDTO.getAllMembers());
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Constructor without Parameters")
    void groupAllMembersDTO_ConstructorWithoutParametersTest() {

        //Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        List<GroupMemberClearanceDTO> listGroupMemberClearanceDTO = new ArrayList<>();
        listGroupMemberClearanceDTO.add(groupMemberClearanceDTO);

        // Act
        GroupAllMembersDTO groupAllMembersDTO = new GroupAllMembersDTO();
        groupAllMembersDTO.setAllMembers(listGroupMemberClearanceDTO);

        // Assert
        assertEquals(listGroupMemberClearanceDTO, groupAllMembersDTO.getAllMembers());
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Equals || Same Object")
    void groupAllMembersDTO_EqualsTest_SameObject() {

        //Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        // Expected

        List<GroupMemberClearanceDTO> listGroupMemberClearanceDTO = new ArrayList<>();
        listGroupMemberClearanceDTO.add(groupMemberClearanceDTO);

        GroupAllMembersDTO expectedGroupAllMembersDTO = new GroupAllMembersDTO(listGroupMemberClearanceDTO);


        // Act
        GroupAllMembersDTO groupAllMembersDTO = new GroupAllMembersDTO();
        groupAllMembersDTO.setAllMembers(listGroupMemberClearanceDTO);

        // Assert
        assertEquals(expectedGroupAllMembersDTO, groupAllMembersDTO);
        assertEquals(groupAllMembersDTO, groupAllMembersDTO); // a object is equals to it self
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Equals || Different Object")
    void groupAllMembersDTO_EqualsTest_DifferentObject() {

        //Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        String bugKiller = "Bug Killer";

        // Expected

        List<GroupMemberClearanceDTO> listGroupMemberClearanceDTO = new ArrayList<>();
        listGroupMemberClearanceDTO.add(groupMemberClearanceDTO);

        GroupAllMembersDTO expectedGroupAllMembersDTO = new GroupAllMembersDTO(listGroupMemberClearanceDTO);


        // Act
        GroupAllMembersDTO groupAllMembersDTO = new GroupAllMembersDTO();

        // Assert
        assertNotEquals(expectedGroupAllMembersDTO, groupAllMembersDTO);
        assertNotEquals(bugKiller, groupAllMembersDTO); // not same instance
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Hash Code")
    void groupAllMembersDTO_HashCodeTest() {

        //Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        List<GroupMemberClearanceDTO> listGroupMemberClearanceDTO = new ArrayList<>();
        listGroupMemberClearanceDTO.add(groupMemberClearanceDTO);

        int expectedHashCode = 1502202214;

        // Act
        GroupAllMembersDTO groupAllMembersDTO = new GroupAllMembersDTO();


        // Assert
        assertNotEquals(expectedHashCode, groupAllMembersDTO.hashCode());
    }

}