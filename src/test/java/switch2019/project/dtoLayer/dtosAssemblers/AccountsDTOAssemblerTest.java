package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.dtoLayer.dtos.AccountDTO;
import switch2019.project.dtoLayer.dtos.AccountsDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountsDTOAssemblerTest {

    @Test
    @DisplayName("AccountsDTOAssembler - Test create AccountsDTO from domain objects")
    void accountsDTOAssembler_createDTOFromDomainObject() {

        //Arrange

        String accountDenomination = "Supermarket Account";
        String accountDescription = "Supermarket expenses";

        AccountDTO accountDTO = AccountDTOAssembler.createDTOFromPrimitiveTypes(accountDenomination, accountDescription);

        List<AccountDTO> listAccountDTO = new ArrayList<>();
        listAccountDTO.add(accountDTO);

        //Act
        AccountsDTOAssembler accountsDTOAssembler = new AccountsDTOAssembler();
        AccountsDTO accountsDTO = accountsDTOAssembler.createDTOFromDomainObject(listAccountDTO);

        //Expected
        AccountsDTO accountsDTOExpected = new AccountsDTO(listAccountDTO);

        //Assert
        assertEquals(accountsDTOExpected, accountsDTO);
    }
}