package switch2019.project.controllerLayer.unitTests.controllersCLI;

class US019GetPersonAndMyGroupsTransactionsWithinAPeriodControllerTest {
/*
    @Test
    @DisplayName("Get all transactions of one person and their groups between two dates | Happy case")
    public void getPersonAndAllGroupsRecords() {

        //Arrange person
        Person personMaria = new Person("Maria", LocalDate.of(1998, 9, 21));

        //Arrange category
        Category categorySalary = new Category("Salary");
        Category categoryFun = new Category("Salary");
        Category categorySupermarket = new Category("House expenses");

        //Arrange Account

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);


        //Arrange of Transaction

            //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

            //Transaction B
        String transactionTypeB = "Debit";
        String transactionDescriptionB = "Friends Dinner of the month";
        double transactionAmountB = 15.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);

            //Transaction C
        String transactionTypeC = "Debit";
        String transactionDescriptionC = "House expenses of food";
        double transactionAmountC = 20.00;
        LocalDate dateC = LocalDate.of(2020, 01, 06);

        // Arrange groups Transactions
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("People who come together to run");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(runnersID, personMaria, runnersDescription, runnersDate);
        Group groupRunnersMaria = personMaria.listOfMyGroups().getListOfGroups().get(0);
        groupRunnersMaria.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);

        Denomination friendsDenomination = new Denomination("Friends");
        GroupID friendsID = new GroupID(friendsDenomination);
        Description friendsDescription = new Description("Old friends from school");
        DateOfCreation friendsDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(friendsID, personMaria, friendsDescription, friendsDate);
        Group groupFriendsMaria = personMaria.listOfMyGroups().getListOfGroups().get(1);
        groupFriendsMaria.createAndAddTransactionWithDate(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);

        //Arrange person Transaction
        personMaria.createAndAddTransactionWithDate(categorySupermarket, transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);

        //Expected
        List<Transaction> expectedRegists = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySupermarket,transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        Transaction expectedTransaction3 = new Transaction(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRegists.add(expectedTransaction1);
        expectedRegists.add(expectedTransaction2);
        expectedRegists.add(expectedTransaction3);

        //Act
        Controller_US019 controller_us019 = new Controller_US019();
        List<Transaction> searchResult = controller_us019.getPersonAndGroupRecordsBetweenTwoDates(personMaria, LocalDate.of(2019, 10,1), LocalDate.of(2020, 01, 29));

        //Assert
        assertEquals(expectedRegists, searchResult);
    }

    @Test
    @DisplayName("Get all transactions of one person and their groups between two dates | One Transaction Outside Range")
    public void getPersonAndAllGroupsRecords_TransactionOutsideRange() {

        //Arrange person
        Person personMaria = new Person("Maria", LocalDate.of(1998, 9, 21));

        //Arrange category
        Category categorySalary = new Category("Salary");
        Category categoryFun = new Category("Salary");
        Category categorySupermarket = new Category("House expenses");

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);

        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //Transaction B
        String transactionTypeB = "Debit";
        String transactionDescriptionB = "Friends Dinner of the month";
        double transactionAmountB = 15.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);

        //Transaction C
        String transactionTypeC = "Debit";
        String transactionDescriptionC = "House expenses of food";
        double transactionAmountC = 20.00;
        LocalDate dateC = LocalDate.of(2020, 01, 06);

        // Arrange groups Transactions
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("People who come together to run");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(runnersID, personMaria, runnersDescription, runnersDate);
        Group groupRunnersMaria = personMaria.listOfMyGroups().getListOfGroups().get(0);
        groupRunnersMaria.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);

        Denomination friendsDenomination = new Denomination("Friends");
        GroupID friendsID = new GroupID(friendsDenomination);
        Description friendsDescription = new Description("Old friends from school");
        DateOfCreation friendsDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(friendsID, personMaria, friendsDescription, friendsDate);
        Group groupFriendsMaria = personMaria.listOfMyGroups().getListOfGroups().get(1);
        groupFriendsMaria.createAndAddTransactionWithDate(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);

        //Arrange person Transaction
        personMaria.createAndAddTransactionWithDate(categorySupermarket, transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);

        //Expected
        List<Transaction> expectedRegists = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySupermarket,transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRegists.add(expectedTransaction1);
        expectedRegists.add(expectedTransaction2);

        //Act
        Controller_US019 controller_us019 = new Controller_US019();
        List<Transaction> searchResult = controller_us019.getPersonAndGroupRecordsBetweenTwoDates(personMaria, LocalDate.of(2020, 1,1), LocalDate.of(2020, 01, 29));

        //Assert
        assertEquals(expectedRegists, searchResult);
    }

    @Test
    @DisplayName("Get all transactions of one person and their groups between two dates | Only Group Transactions")
    public void getPersonAndAllGroupsRecords_OnlyGroupTransactions() {

        //Arrange person
        Person personMaria = new Person("Maria", LocalDate.of(1998, 9, 21));

        //Arrange category
        Category categorySalary = new Category("Salary");
        Category categoryFun = new Category("Salary");
        Category categorySupermarket = new Category("House expenses");

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);

        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //Transaction B
        String transactionTypeB = "Debit";
        String transactionDescriptionB = "Friends Dinner of the month";
        double transactionAmountB = 15.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);

        // Arrange groups Transactions
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("People who come together to run");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(runnersID, personMaria, runnersDescription,runnersDate);
        Group groupRunnersMaria = personMaria.listOfMyGroups().getListOfGroups().get(0);
        groupRunnersMaria.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);

        Denomination friendsDenomination = new Denomination("Friends");
        GroupID friendsID = new GroupID(friendsDenomination);
        Description friendsDescription = new Description("Old friends from school");
        DateOfCreation friendsDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(friendsID, personMaria, friendsDescription, friendsDate);
        Group groupFriendsMaria = personMaria.listOfMyGroups().getListOfGroups().get(1);
        groupFriendsMaria.createAndAddTransactionWithDate(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);

        //Expected
        List<Transaction> expectedRegists = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRegists.add(expectedTransaction1);
        expectedRegists.add(expectedTransaction2);

        //Act
        Controller_US019 controller_us019 = new Controller_US019();
        List<Transaction> searchResult = controller_us019.getPersonAndGroupRecordsBetweenTwoDates(personMaria, LocalDate.of(2019, 10,1), LocalDate.of(2020, 01, 29));

        //Assert
        assertEquals(expectedRegists, searchResult);
    }

    @Test
    @DisplayName("Get all transactions of one person and their groups between two dates | Only Person Transactions")
    public void getPersonAndAllGroupsRecords_OnlyPersonTransactions() {

        //Arrange person
        Person personMaria = new Person("Maria", LocalDate.of(1998, 9, 21));

        //Arrange category
        Category categorySalary = new Category("Salary");
        Category categoryFun = new Category("Salary");
        Category categorySupermarket = new Category("House expenses");

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);

        //Arrange of Transaction

        //Transaction
        String transactionType = "Debit";
        String transactionDescription = "House expenses of food";
        double transactionAmount = 20.00;
        LocalDate date = LocalDate.of(2020, 01, 06);

        //Arrange person Transaction
        personMaria.createAndAddTransactionWithDate(categorySupermarket, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);

        //Expected
        List<Transaction> expectedRegists = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySupermarket,transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        expectedRegists.add(expectedTransaction1);

        //Act
        Controller_US019 controller_us019 = new Controller_US019();
        List<Transaction> searchResult = controller_us019.getPersonAndGroupRecordsBetweenTwoDates(personMaria, LocalDate.of(2019, 10,1), LocalDate.of(2020, 01, 29));

        //Assert
        assertEquals(expectedRegists, searchResult);
    }

    @Test
    @DisplayName("Get all transactions of one person and their groups between two dates | No Transactions Within Range")
    public void getPersonAndAllGroupsRecords_NoTransactionsWithinRange() {

        //Arrange person
        Person personMaria = new Person("Maria", LocalDate.of(1998, 9, 21));

        //Arrange category
        Category categorySalary = new Category("Salary");
        Category categoryFun = new Category("Salary");
        Category categorySupermarket = new Category("House expenses");

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);

        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //Transaction B
        String transactionTypeB = "Debit";
        String transactionDescriptionB = "Friends Dinner of the month";
        double transactionAmountB = 15.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);

        //Transaction C
        String transactionTypeC = "Debit";
        String transactionDescriptionC = "House expenses of food";
        double transactionAmountC = 20.00;
        LocalDate dateC = LocalDate.of(2020, 01, 06);

        // Arrange groups Transactions
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("People who come together to run");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(runnersID, personMaria, runnersDescription, runnersDate);
        Group groupRunnersMaria = personMaria.listOfMyGroups().getListOfGroups().get(0);
        groupRunnersMaria.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);

        Denomination friendsDenomination = new Denomination("Friends");
        GroupID friendsID = new GroupID(friendsDenomination);
        Description friendsDescription = new Description("Old friends from school");
        DateOfCreation friendsDate = new DateOfCreation(LocalDate.of(2019, 12, 18));
        personMaria.createGroupAsPeopleInCharge(friendsID, personMaria, friendsDescription, friendsDate);
        Group groupFriendsMaria = personMaria.listOfMyGroups().getListOfGroups().get(1);
        groupFriendsMaria.createAndAddTransactionWithDate(categoryFun, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);

        //Arrange person Transaction
        personMaria.createAndAddTransactionWithDate(categorySupermarket, transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);

        //Expected
        List<Transaction> expectedRegists = new ArrayList<>();

        //Act
        Controller_US019 controller_us019 = new Controller_US019();
        List<Transaction> searchResult = controller_us019.getPersonAndGroupRecordsBetweenTwoDates(personMaria, LocalDate.of(2020, 02,1), LocalDate.of(2020, 02, 29));

        //Assert
        assertEquals(expectedRegists, searchResult);
    }

 */
}