package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US002_1CreateGroupService;
import switch2019.project.dtoLayer.dtos.CreateGroupDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US002_1CreateGroupController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.vosShared.DateOfCreation;
import switch2019.project.domainLayer.domainEntities.vosShared.Denomination;
import switch2019.project.domainLayer.domainEntities.vosShared.Description;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class US002_1CreateGroupControllerTest extends AbstractTest {

    //US002.1 Como utilizador, quero criar grupo, tornando-me administrador do grupo.

    @Mock
    private US002_1CreateGroupService us002_1CreateGroupService;

    @Test
    @DisplayName("As a person (user), create group where one is in charge - Happy path")
    public void createGroupAsPersonInCharge_HappyPath() {

        // Arrange
        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Vale Family";
        String groupDescription = "All members from Vale family";

        //Expected GroupDTO

        Denomination denomination = Denomination.createDenomination(groupDenomination);
        Description description = Description.createDescription(groupDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());
        GroupDTO isGroupCreated = GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        // DTO
        CreateGroupDTO createGroupDTO = CreateGroupDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, groupDescription);

        Mockito.when(us002_1CreateGroupService.createGroupAsPersonInCharge(createGroupDTO)).thenReturn(isGroupCreated);

        // Act
        US002_1CreateGroupController us002_1CreateGroupController = new US002_1CreateGroupController(us002_1CreateGroupService);
        GroupDTO result = us002_1CreateGroupController.createGroupAsPersonInCharge(personEmail, groupDenomination, groupDescription);

        //Assert
        assertEquals(isGroupCreated, result);
    }

    @Test
    @DisplayName("As a person (user), create group where one is in charge - Sad path (attempt to create group that already exists)")
    public void createGroupAsPersonInCharge_GroupAlreadyExists() {

        // Arrange
        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";

        // DTO
        CreateGroupDTO createGroupDTO = CreateGroupDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, groupDescription);

        Mockito.when(us002_1CreateGroupService.createGroupAsPersonInCharge(createGroupDTO)).thenThrow(new InvalidArgumentsBusinessException(US002_1CreateGroupService.GROUP_ALREADY_EXISTS));

        // Controller
        US002_1CreateGroupController us002_1CreateGroupController = new US002_1CreateGroupController(us002_1CreateGroupService);

        //Act

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us002_1CreateGroupController.createGroupAsPersonInCharge(personEmail, groupDenomination, groupDescription));

        //Assert
        assertEquals(thrown.getMessage(), US002_1CreateGroupService.GROUP_ALREADY_EXISTS);
    }

    @Test
    @DisplayName("As a person (user), create group where one is in charge - Sad path (person does not exist in repository)")
    public void createGroupAsPersonInCharge_PersonDoesNotExists() {

        // Arrange
        String personEmail = "xana@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";

        // DTO
        CreateGroupDTO createGroupDTO = CreateGroupDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, groupDescription);

        Mockito.when(us002_1CreateGroupService.createGroupAsPersonInCharge(createGroupDTO)).thenThrow(new NotFoundArgumentsBusinessException(US002_1CreateGroupService.PERSON_DOES_NOT_EXIST));

        // Act
        US002_1CreateGroupController us002_1CreateGroupController = new US002_1CreateGroupController(us002_1CreateGroupService);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us002_1CreateGroupController.createGroupAsPersonInCharge(personEmail, groupDenomination, groupDescription));

        //Assert
        assertEquals(thrown.getMessage(), US002_1CreateGroupService.PERSON_DOES_NOT_EXIST);
    }
}